<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>
    <link rel="shortcut icon" href="http://helow.akaarit.xyz/backend/images/favicon.ico">

    <!-- Custom fonts for this template-->
    <link href="{{  asset('/')}}/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{  asset('/')}}/assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

</head>

<body class="bg-gradient-info" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');">
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Home</span>
                
              </a>
            </li>

          </ul>

        </nav>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-6 col-lg-6 col-md-6 p-5">

        <div class="card o-hidden border-0 shadow-lg my-3" style="background-color: #2F4F4F">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
           
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 mb-4" style="color: white">Admin Login</h1>
                  </div>
                    <form method="POST" action="{{ route('admin.login.submit') }}" class="user" aria-label="{{ __('Admin Login') }}">
                         {{ csrf_field()}}

                        <div class="form-group">
                                <input id="email" type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                           
                                <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter Your password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                          <div class="custom-control custom-checkbox small">
                            <input type="checkbox" name="remember" id="remember"  class="custom-control-input">
                            <label class="custom-control-label" for="remember">Remember Me</label>
                          </div>
                        </div>

                       
                        <button type="submit" class="btn btn-info btn-user btn-block">
                            {{ __('Admin Login') }}
                        </button>
                    </form>
               
               
                  
                   
                 
                </div>
              </div>
            </div>
      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>