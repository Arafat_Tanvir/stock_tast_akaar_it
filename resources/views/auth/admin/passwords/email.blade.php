<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>{{ config('app.name', 'Laravel') }}</title>

      <!-- Custom fonts for this template-->
      <link href="{{ asset('Backend/Admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

      <link href="{{ asset('backend/admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

      <!-- Custom styles for this template-->
      <link href="{{ asset('Backend/Admin/css/sb-admin-2.min.css')}}" rel="stylesheet">


      <!-- CkEditor -->
      <script src="{{  asset('/')}}ckeditor/ckeditor.js"></script>
      <script src="{{  asset('/')}}ckeditor/samples/js/sample.js"></script>
      <!--  -->
      <link rel="stylesheet" href="{{  asset('/')}}ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">

      <!--end CkEditor-->


</head>

<body class="bg-gradient-info" style="background-image: url('{{ asset('/') }}assets/frontend/images/bg_1.jpg');">
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="{{ route('/')}}" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Home</span>
                
              </a>
            </li>

          </ul>

        </nav>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
      

      <div class="col-xl-6 col-lg-6 col-md-6 p-5">

        <div class="card o-hidden border-0 shadow-lg my-3">
          
          <div class="card-body p-0">
             
           
                <div class="p-5">
                  @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                  @endif
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Admin reset password</h1>
                  </div>
                     <form method="POST" class="user" action="{{ route('admin.password.email') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf

                        <div class="form-group">       
                                <input id="email" type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" required placeholder="Enter Your Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <button type="submit" class="btn btn-info btn-user btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </form>
                </div>
              </div>
            </div>
      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>