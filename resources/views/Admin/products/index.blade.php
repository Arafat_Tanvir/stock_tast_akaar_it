@extends('admin.layouts.master')

@section('content')

<div class="row">
	
	<div class="col-sm-12">
	
<div class="card shadow mb-4">
            
            <div class="card-body">
              <h1 class="text-right"><a href="#Savaproduct" data-toggle="modal"><i class="fas fa-backward"></i>Add product</a></h1>
				<div class="modal fade" id="Savaproduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-producter">
								<h5 class="modal-title" id="exampleModalLabel">Product Save</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form method="POST" action="{{ route('product_store') }}">
							         {{ csrf_field()}}
							          <div class="form-group">
							              <label for="name">Product Name</label>
							              <div class="form-input">
							                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="name" id="name" placeholder="Enter Head Name" value="{{old('name')}}" required>
							                  <div class="valid-feedback">
							                    {{ ($errors->has('name')) ? $errors->first('name') : ''}}
							                  </div>
							              </div>
							          </div>

							          <div class="form-group">
							              <label for="price">Product Price</label>
							              <div class="form-input">
							                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="price" id="price" placeholder="Enter Head price" value="{{old('price')}}" required>
							                  <div class="valid-feedback">
							                    {{ ($errors->has('price')) ? $errors->first('price') : ''}}
							                  </div>
							              </div>
							          </div>

							          <div class="form-group">
							              <label for="quantity">Product Quantity</label>
							              <div class="form-input">
							                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="quantity" id="quantity" placeholder="Enter Head quantity" value="{{old('quantity')}}" required>
							                  <div class="valid-feedback">
							                    {{ ($errors->has('quantity')) ? $errors->first('quantity') : ''}}
							                  </div>
							              </div>
							          </div>

							          <button class="btn btn-primary float-right" type="submit">Save</button>
							      </form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</a>
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="categories" class="table table-bordered table-striped">
				          <caption>List of product</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Name</th>
				  						<th>Price</th>
				  						<th>Quantity</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($products as $product)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $product->name }}</td>
				  						<td class="text-center">{{ $product->price }}</td>
				  						<td class="text-center">{{ $product->quantity }}</td>
				  						<td class="text-center">
				                            <a href="#DeleteModal{{$product->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-producter">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('product_delete', $product->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>

@endsection