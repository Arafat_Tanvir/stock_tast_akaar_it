@extends('admin.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>7 Days meal</h1>
            </div>

            <div class="card-body">
             <!--  <h1 class="text-right"><a href="{{route('schedule_index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2"> -->
	      	<form method="POST" action="{{ route('schedule_store') }}">
	         {{ csrf_field()}}

	          <div class="form-group">
	              <label for="start_date">From Date</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="start_date" id="start_date" placeholder="Enter Credit Date" value="{{old('start_date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('start_date')) ? $errors->first('start_date') : ''}}
	                  </div>
	              </div>
	          </div>

	           <div class="form-group">
	              <label for="end_date">To Date</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="end_date" id="end_date" placeholder="Enter Credit Date" value="{{old('end_date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('end_date')) ? $errors->first('end_date') : ''}}
	                  </div>
	              </div>
	          </div>

	          <button class="btn btn-primary float-right" type="submit">Add Date</button>
	      </form>

	      <div class="mt-2">
              	 <form method="POST" action="{{ route('meal_store') }}"  enctype="multipart/form-data" >
                  {{ csrf_field()}}

              	<div class="table-responsive mt-2">
				        <table id="categories" class="table table-bordered table-striped">
				          <caption>List of Food</caption>
				          <thead>
				  					<tr>
				  						<th>Meal</th>
				  						@foreach($schedules as $schedule)
					  					<th>{{ $schedule}}</th>
					  					@endforeach
				  					</tr>
				  				</thead>
				  				<tbody>
				  					 @foreach($schedules as $schedule)
				  					     <input type="text" name="date[]" class="invisible" value="{{ $schedule }}"/>
				  					 @endforeach
				  					 
				  					@foreach($eats as $eat)
				  					<tr>
				  						   <td style="background: green;color: white">
				  						   	{{$eat->name}}
				  						   </td>
				  						   <input type="text" name="eat_id[]" class="invisible" value="{{ $eat->id }}"/>
				  						   @foreach($schedules as $schedule)
				  						   
				  						   <td class="text-left">
				  						   
					  						@foreach($foods as $food)
					  							<!-- <input type="text" name="date" value="{{ $schedule }}"/> -->
					  							 <input type="checkbox" class=""  name="name{{$schedule}}{{$eat->id}}[]" value="{{ $food->name }}" /> {{ $food->name }}
					  							 <br>
					  						@endforeach
					  					    </td>
					  					    @endforeach
				  						
				  					</tr>
				  					@endforeach
				  					
				  				</tbody>
				        </table>
				      </div>
				       <button class="btn btn-primary float-right" type="submit">Save</button>
		            </form>
	      	
		    </div>
	    </div>
	  </div>
	</div>
	</div>
	<div class="col-sm-2">
		
	</div>
	</div>


	@endsection

@section('scripts')
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
	<script>
	    $(function(){
	      $("#start_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	      $("#end_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	    });
	</script>
@endsection