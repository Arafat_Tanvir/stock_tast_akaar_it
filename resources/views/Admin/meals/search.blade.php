@extends('admin.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Meal Search</h1>
            </div>
            <div class="card-body">
	      	<form method="POST" action="{{ route('search_by') }}">
	         {{ csrf_field()}}

	          <div class="form-group">
	              <label for="date">Search by date</label>
	              <div class="form-input">
	                  <input type="text" class="form-control" name="date" id="date" placeholder="Enter Credit Date" value="{{old('date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('date')) ? $errors->first('date') : ''}}
	                  </div>
	              </div>
	          </div>

	          <button class="btn btn-primary float-right" type="submit">Search</button>
	      </form>


	    </div>
	    <div class="card-body">
	    	
	   
	     <table id="categories" class="table table-bordered table-striped">
				          <caption>List of Food</caption>
				          <thead>
				  					<tr>
				  						<th>Meal Type</th>
				  						<th>Food List</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					@foreach($eats as $eat)
				  					<tr>
				  						   <td style="background: green;color: white">
				  						   	{{$eat->name}}
				  						   </td>
				  						   <td>
				  						   	@if(isset($date))
				  						   	@php
				  						   	
				  						    	$meal=App\Models\Meal::orderBy('id','desc')->where('date',$date)->where('eat_id',$eat->id)->first();
				  						   	@endphp

					  						   	@if(isset($meal->name))
												 {{$meal->name}}
												@else

											    @endif

										    @else

										    @endif


				  						   </td>
				  						
				  					</tr>
				  					@endforeach
				  					
				  				</tbody>
				        </table>
				         </div>
	  </div>
	</div>
	</div>
	

	@endsection

@section('scripts')
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
	<script>
	    $(function(){
	      $("#date").datepicker({dateFormat: "yy-mm-dd"}).val();
	      $("#end_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	    });
	</script>
@endsection