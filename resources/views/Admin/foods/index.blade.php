@extends('admin.layouts.master')

@section('content')

<div class="row">
	
	<div class="col-sm-12">
	
<div class="card shadow mb-4">
            
            <div class="card-body">
              <h1 class="text-right"><a href="#SavaFood" data-toggle="modal"><i class="fas fa-backward"></i>Add Food</a></h1>
					<div class="modal fade" id="SavaFood" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-fooder">
									<h5 class="modal-title" id="exampleModalLabel">Food Save</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="POST" class="user" action="{{ route('food_store') }}">
								         {{ csrf_field()}}
								          <div class="form-group">
								              <label for="name">Food Name</label>
								              <div class="form-input">
								                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="name" id="name" placeholder="Enter Head Name" value="{{old('name')}}" required>
								                  <div class="valid-feedback">
								                    {{ ($errors->has('name')) ? $errors->first('name') : ''}}
								                  </div>
								              </div>
								          </div>

								          <button class="btn btn-primary float-right" type="submit">Save</button>
								      </form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
		
              <div class="mt-2">
				        <table id="food" class="table table-bordered table-striped">
				          <caption>List of Food</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Name</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<div style="display: none;">{{$a=1}}</div>
				  						@foreach($foods as $food)
				  					<tr>
				  						
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $food->name }}</td>
				  						<td class="text-center">
				                            <a href="#DeleteModal{{$food->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$food->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-fooder">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('food_delete', $food->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
	    </div>
	  </div>
	</div>
	</div>
	
	</div>



@endsection