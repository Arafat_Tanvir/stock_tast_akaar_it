@extends('admin.layouts.master')

@section('content')


<div class="row">
	
	<div class="col-sm-12">
	<form method="POST" action="{{ route('order_store') }}"  enctype="multipart/form-data" >
                  {{ csrf_field()}}
<div class="card shadow mb-4">
	         
            <div class="card-body">

            <div class="row">
	          	<div class="col-sm-4">
	          <div class="form-group">
	              <label for="budget">Budget</label>
	              <div class="form-input">
	                  <input type="text" class="form-control" name="budget" id="budget" placeholder="Enter budget" value="{{old('budget')}}" required >
	                  <div class="valid-feedback">
	                    {{ ($errors->has('budget')) ? $errors->first('budget') : ''}}
	                  </div>
	              </div>
	          </div>

	         
	            </div>
	          	<div class="col-sm-4">
	          		
	          		<h4 >Budget <span id="total_budget_select"></span> taka
	          		</h4>
	          	</div>
	          	<div class="col-sm-4">

	          		<h4 > Total <input type="text" class="form-control" name="total_amount" id="total_amount"  value="" required > Amount</h4>
	          	
	          </div>
	          </div>
	          <br>
	          <p id="show" style="color: red" class="text-align:center"> Please select less then budget</p>
	          <br>
	         <table class="table table-bordered table-striped" id="myTable">
	         	<thead>
	         		<tr>
	         			<th>Name</th>
	         			<th>Quantity</th>
	         			<th>Price</th>
	         			<th>Total Price</th>
	         			<td><a href="javascript:void(0);" class="add_button btn btn-success form-control" title="Add field">Add</a></td>
	         		</tr>
	         	</thead >
	         	
	         	    <tbody class="field_wrapper" id="field_wrapper">
	         	    </tbody>
	         	   
	         </table>
		</div>
	</div>
	 <button class="btn btn-primary float-right" type="submit">Save</button>
	    
              </form>
</div>
</div>



@endsection

@section('scripts')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#show").hide();

		$('#budget').keyup('change',function(){
			var budget=$(this).val();
			 $('#total_budget_select').text(budget);
		});

		$(".field_wrapper").delegate(".quantity","keyup",function(){
			var quantity=$(this).val();

		 	var rt=$(this).parent().parent();
		 	unit_price=rt.find("#unit_price").val();
		 	var total=quantity * unit_price;
		 	if (total!=null) {
		 		rt.find('#total_price').val(total);
		 		calculate();

		 	}else{
		 		alert("error");
		 	}
		 	
		});

		function calculate(){
			var total_amount=0;
			$("#total_amount").empty();
			$.each(total_price , function(key, val) { 
				//console.log($(this).val() * 1);
				total_amount = total_amount + ($(this).val() * 1);
			});
			//alert(total_amount);
			$('#total_amount').val(total_amount);
		}


		 $('.field_wrapper').on('change', '.product_id', function () { 
		 	var product_id=$(this).val();
		 	var tr=$(this).parent().parent();
		 	if(product_id)
			{
				$.ajax({
				url:'{{ url('')}}/product/ajax/'+product_id,
				type:"GET",
				dataType:"json",
					success:function(data)
					{

						tr.find('#unit_price').val(data);

						var unit_price=tr.find("#unit_price").val();
						var quantity=tr.find("#quantity").val();
		 	            var total=quantity * unit_price;
		 	            tr.find('#total_price').val(total);
		 	            calculate();

					}
			   });
			}else
			{
				$('#floor_id').empty();
			}
		 	
		 	});
	});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 100; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<tr id="cur_tr"> <td> <select name="product_id[]" id="product_id" class="form-control product_id"> <option  selected> ====select product====</option> @foreach($products as $product)  <option value="{{ $product->id}}">{{ $product->name}}({{ $product->quantity}})</option> @endforeach </select> </td> <td> <input type="number" id="quantity" class="form-control quantity" name="quantity[]" value=""/> </td>  <td><input type="text" class="form-control unit_price" id="unit_price" name="unit_price[]" value=""/> </td><td><input type="text" class="form-control total_price" name="total_price[]" id="total_price" value=""/> </td><td href="javascript:void(0);" class="remove_button"><button class="btn btn-danger form-control">Remove</button><td></tr>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('tr').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>





@endsection