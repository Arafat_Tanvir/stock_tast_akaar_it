@extends('admin.layouts.master')

@section('content')

<div class="row">
	
	<div class="col-sm-12">
	
<div class="card shadow mb-4">
            <div class="card-body">
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="" class="table table-bordered table-striped">
				          <caption>List of order</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Name</th>
				  						<th>Total Amount</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<div style="display: none;">{{$a=1}}</div>
				  					@foreach($orders as $order)
				  					<tr>
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $order->admin->name }}</td>
				  						<td class="text-center">{{ $order->total_amount}}</td>
				  						<td class="text-center">
				                             <a href="{{route('order_show', $order->id)}}" class="badge badge-primary">Show</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>

@endsection

@section('scripts')
<script>
	$(document).ready(function() {
    $('#categories').DataTable();
} );
</script>

@endsection