@extends('admin.layouts.master')

@section('content')

<div class="row">
	
	<div class="col-sm-12">
	
<div class="card shadow mb-4">
            
            <div class="card-body">
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="categories" class="table table-bordered table-striped">
				          <caption>List of Food</caption>
				          <thead class="text-center">
				  					<tr>
				  						
				  						<th>Product Name</th>
				  						<th>Quantity</th>
				  						<th>Price</th>
				  						<th>Total Price</th>
				  						
				  					</tr>
				  				</thead>
				  				<tbody class="text-center">
				  					@foreach(App\Models\OrderDetail::orderBy('id','desc')->where('order_id',$order->id)->get() as $order)
				  					<tr>
				  						<td class="text-center">{{ $order->product->name}}</td>
				  						<td class="text-center">{{ $order->quantity }}</td>
				  						<td class="text-center">{{ $order->unit_price }}</td>
				  						<td class="text-center">{{ $order->total_price }}</td>
				  						
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>

@endsection