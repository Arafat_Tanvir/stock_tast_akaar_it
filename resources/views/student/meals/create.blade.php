@extends('student.layouts.master')

@section('content')
<style type="text/css">
	.card-header.shadow.design {
    background: black;
    color: white;
}
</style>
<div class="row">
	<div class="col-sm-12">
        <div class="card shadow">
        	
        	<div class="card-header shadow design">
        		
	        	<h4>Student : {{ Auth::user()->name}}</h4>
	             
              </div>
            <div class="card-header text-center">
              <h4 style="color: green;">7 days Meals</h4>
            </div>
           
            <div class="card-body">
              <div class="">
              	 <form method="POST" action="{{ route('student_meal_store') }}"  enctype="multipart/form-data" >
                  {{ csrf_field()}}

              	     <div class="table-responsive mt-2">
				        <table id="categories" class="table table-bordered table-striped">
				          <caption>List of Food</caption>
				          <thead>
				  					<tr>
				  						<th>Meal</th>
				  						@foreach($schedules as $schedule)
					  					<th>{{ $schedule}}</th>
					  					@endforeach
				  					</tr>
				  				</thead>
				  				<tbody>
				  					 @foreach($schedules as $schedule)
				  					     <input type="text" name="date[]" class="invisible" value="{{ $schedule }}"/>
				  					 @endforeach

				  					@foreach($eats as $eat)
				  					<tr>
				  						   <td style="background: green;color: white">
				  						   	{{$eat->name}}
				  						   </td>
				  						   <input type="text" name="eat_id[]" class="invisible" value="{{ $eat->id }}"/>
				  						   @foreach($schedules as $schedule)
				  						   
				  						   <td class="text-left">

				  						   	 @php
				  						   	
				  						   	 $foods =explode(',',App\Models\Meal::orderBy('id','desc')->where('date',$schedule)->where('eat_id',$eat->id)->first()->name);
				  						   	
				  						     
				  						   	 @endphp

				  						   	 @foreach($foods as $d)

				  						   	     <input type="radio" name="name{{$schedule}}{{$eat->id}}" value="{{ $d }}">  {{ $d}}
				  						   	     <br>
				  						       
				  						   	 @endforeach
				  						   	 	



                                            

					  					    </td>
					  					   
					  					    @endforeach
				  						
				  					</tr>
				  					@endforeach
				  					
				  				</tbody>
				        </table>
				      </div>
				       <button class="btn btn-primary float-right" type="submit">Save</button>
		            </form>
	      	
		    </div>
		  
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>
	@endsection