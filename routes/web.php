<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [
	'uses' => 'Student\StudentController@dashboard',
	'as' => 'student-dashboard'
]);

Auth::routes();
Route::get('/home', [
	'uses' => 'Student\StudentController@dashboard',
	'as' => 'home'
]);

Route::get('/product/ajax/{id}', [
		'uses' => 'admin\MealController@load_product',
		'as' => 'product_load'
	]);
// Route::get('/home', 'HomeController@index')->name('home');




Route::group(['prefix'=>'admin'],function(){

		Route::get('/', [
			'uses' => 'Admin\AdminController@dashboard',
			'as' => 'admin.dashboard'
		]);
	    //admin login and logout
		Route::get('/login',[
			'uses'=>'Auth\Admin\LoginController@showLoginForm',
			'as'=>'admin.login'
		]);

		Route::post('/login/submit',[
			'uses'=>'Auth\Admin\LoginController@login',
			'as'=>'admin.login.submit'
		]);

		Route::post('/logout',[
			'uses'=>'Auth\Admin\LoginController@logout',
			'as'=>'admin.logout'
	    ]);

	    Route::group(['prefix'=>'meals'],function(){
		Route::get('/',"admin\MealController@index")->name('meal_index');
		Route::get('/search',"admin\MealController@search")->name('meal_search');
		Route::post('/store',"admin\MealController@store")->name('meal_store');
		Route::post('/search_by',"admin\MealController@search_by")->name('search_by');

		Route::group(['prefix'=>'foods'],function(){
			Route::get('/',"admin\FoodController@index")->name('food_index');
			Route::get('/create',"admin\FoodController@create")->name('food_create');
			Route::post('/store',"admin\FoodController@store")->name('food_store');
			Route::post('/delete/{id}',"admin\FoodController@delete")->name('food_delete');
		});

		Route::group(['prefix'=>'schedules'],function(){
			Route::get('/',"admin\ScheduleController@index")->name('schedule_index');
			Route::get('/create',"admin\ScheduleController@create")->name('schedule_create');
			Route::post('/store',"admin\ScheduleController@store")->name('schedule_store');
			Route::post('/delete/{id}',"admin\ScheduleController@delete")->name('schedule_delete');
		});



		Route::group(['prefix'=>'products'],function(){
			Route::get('/',"admin\ProductController@index")->name('product_index');
			Route::get('/create',"admin\ProductController@create")->name('product_create');
			Route::post('/store',"admin\ProductController@store")->name('product_store');
			Route::post('/delete/{id}',"admin\ProductController@delete")->name('product_delete');
		});

		Route::group(['prefix'=>'orders'],function(){
			Route::get('/',"admin\OrderController@index")->name('order_index');
			Route::get('/orderDetails_index',"admin\OrderController@orderDetails_index")->name('orderDetails_index');
			Route::get('/create',"admin\OrderController@create")->name('order_create');
			Route::get('/show/{id}',"admin\OrderController@show")->name('order_show');
			Route::post('/store',"admin\OrderController@store")->name('order_store');
			Route::post('/delete/{id}',"admin\OrderController@delete")->name('order_delete');
		});
});



});

Route::group(['prefix'=>'student'],function()
{
	    //student dashboard route
		Route::get('/', [
			'uses' => 'Student\StudentController@dashboard',
			'as' => 'student-dashboard'
		]);

		Route::group(['prefix'=>'meals'],function(){
			Route::get('/',"Student\MealController@index")->name('student_meal_index');
			Route::post('/store',"Student\MealController@store")->name('student_meal_store');
			
        });
});

