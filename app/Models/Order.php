<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class Order extends Model
{
     public function admin(){

	    return $this->belongsTo(Admin::class);

	  }
}
