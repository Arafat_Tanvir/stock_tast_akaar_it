<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
	{
	    $products =Product::orderBy('id','desc')->get();
	    return view('admin.orders.index',compact('products'));
	}

	


    public function orderDetails_index()
	{
	    $orders =Order::orderBy('id','desc')->get();
	    return view('admin.orders.detail',compact('orders'));
	}

	public function store(Request $request)
	{
		//dd($request);
		$this->validate($request,[
			'product_id'=>'required'
		]);

		if ($request->isMethod('post')) {
			$order=new order();
			$order->budget_price=$request->budget;
			$order->total_amount=$request->total_amount;

			if (Auth::check()) {
	            $order->admin_id=Auth::id();
	        }
			$order->save();
			$data=$request->all();
			foreach ($data['product_id'] as $key => $value) {
				$orderDetails=new OrderDetail();
				$orderDetails->product_id=$value;
				$orderDetails->order_id=$order->id;
				$orderDetails->quantity=$data['quantity'][$key];
				$orderDetails->unit_price=$data['unit_price'][$key];
				$orderDetails->total_price=$data['total_price'][$key];
				$orderDetails->save();
			}
		}

		
		if(!is_null($orderDetails)){
			session()->flash('success','order create Successfully!!');
			return redirect()->route('order_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}

	public function show($id)
      {
           $order=Order::findOrFail($id);
           //dd($order);
           return view('admin.orders.show',compact('order'));
      }

	public function delete($id)
	{
	    $order=Order::find($id);
	    if(!is_null($order))
	    {
	        $order->delete();
	        session()->flash('success','order Delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
