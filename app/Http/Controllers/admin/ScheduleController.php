<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Schedule;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
class ScheduleController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
	{
		
	    $schedules =Schedule::orderBy('id','desc')->get();
	    return view('admin.schedules.index',compact('schedules'));
	}

	public function create()
	{
	    return view('admin.schedules.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'start_date'=>'required',
			'end_date'=>'required'
		]);

		$CarbonPeriod = CarbonPeriod::create($request->start_date, $request->end_date);
	    $schedules = array();
        foreach ($CarbonPeriod as $date) {
            $schedules[] = $date->format('Y-m-d');
        }
        if(count($schedules)==7){
        	$schedule=new Schedule();
			$schedule->start_date=$request->start_date;
			$schedule->end_date=$request->end_date;
			//dd($schedule);
			$schedule->save();
			if(!is_null($schedule)){
				session()->flash('success','schedule create Successfully!!');
				return redirect()->route('meal_index');
			}else{
				session()->flash('stickly_error','Some Error Occer!!');
				return back();
			}
        }else{
				session()->flash('stickly_error','Please Select only 7 days meal');
				return back();
			}

		
	}

	public function delete($id)
	{
	    $schedule=Schedule::find($id);
	    if(!is_null($schedule))
	    {
	        $schedule->delete();
	        session()->flash('success','schedule Delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
