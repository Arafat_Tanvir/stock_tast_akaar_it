<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Meal;
use App\Models\Food;
use App\Models\Schedule;
use App\Models\Product;
use App\Models\Eat;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use DB;

class MealController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function load_product($id)
    {
        $product = Product::where('id', $id)->pluck('price','id')->first();
        return json_encode($product);
    }


    public function index()
	{
		
	    $schedule =Schedule::orderBy('id','desc')->first();
	    $CarbonPeriod = CarbonPeriod::create($schedule->start_date, $schedule->end_date);
	    $schedules = array();
        foreach ($CarbonPeriod as $date) {
            $schedules[] = $date->format('Y-m-d');
        }
	    $foods =Food::orderBy('id','asc')->get();
	    $eats =Eat::orderBy('id','asc')->get();
	    return view('admin.meals.create',compact('schedules','foods','eats'));
	}

	public function search(){
		 $eats =Eat::orderBy('id','asc')->get();
		return view('admin.meals.search',compact('eats'));
	}

	public function search_by(Request $request)
	{
		$this->validate($request,[
			'date'=>'required',
		]);

		$date=$request->date;
		$eats =Eat::orderBy('id','asc')->get();
		return view('admin.meals.search',compact('date','eats'));
	}


	public function store(Request $request)
	{
		// $this->validate($request,[
		// 	'name'=>'required|max:50|min:2|unique:meal',
		// ]);
		$data=$request->all();
		foreach($data['date'] as $key => $value) {

		    	foreach($data['eat_id'] as $key => $val) {
		    		if(isset($data['name'.$value.$val])){
			    		$meal=new Meal();
			    		$meal->name=implode(',', $data['name'.$value.$val]);
						$meal->date=$value;
						$meal->eat_id=$val;
						$meal->save();
					}
		    	}	
		    }
		session()->flash('success','meal create Successfully!!');
		return redirect()->route('meal_index');
	}

	public function delete($id)
	{
	    $meal=Meal::find($id);
	    if(!is_null($meal))
	    {
	        $meal->delete();
	        session()->flash('success','meal Delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
