<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
	{
	    $products =Product::orderBy('id','desc')->get();
	    return view('admin.products.index',compact('products'));
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'name'=>'required|max:50|min:2|unique:products',
		]);

		$product=new Product();
		$product->name=$request->name;
		$product->price=$request->price;
		$product->quantity=$request->quantity;
		$product->save();
		if(!is_null($product)){
			session()->flash('success','product create Successfully!!');
			return redirect()->route('product_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}

	public function delete($id)
	{
	    $product=Product::find($id);
	    if(!is_null($product))
	    {
	        $product->delete();
	        session()->flash('success','product Delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
