<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Food;

class FoodController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    
    public function index()
	{
	    $foods =Food::orderBy('id','desc')->get();
	    return view('admin.foods.index',compact('foods'));
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'name'=>'required|max:50|min:2|unique:food',
		]);

		$food=new Food();
		$food->name=$request->name;
		$food->save();
		if(!is_null($food)){
			session()->flash('success','food create Successfully!!');
			return redirect()->route('food_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}

	public function delete($id)
	{
	    $food=Food::find($id);
	    if(!is_null($food))
	    {
	        $food->delete();
	        session()->flash('success','Food Delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
