<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    


    //for user details and got data for active user and edit user data
    public function dashboard(){
    	return view('admin.index');
    }
}
