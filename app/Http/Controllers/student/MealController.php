<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Stock;
use App\Models\Food;
use App\Models\Schedule;
use App\Models\Eat;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;

class MealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
	{
		
	    $schedule =Schedule::orderBy('id','desc')->first();
	    $CarbonPeriod = CarbonPeriod::create($schedule->start_date, $schedule->end_date);
	    $schedules = array();
        foreach ($CarbonPeriod as $date) {
            $schedules[] = $date->format('Y-m-d');
        }
	    $foods =Food::orderBy('id','asc')->get();
	    $eats =Eat::orderBy('id','asc')->get();
	    return view('student.meals.create',compact('schedules','foods','eats'));
	}

	public function store(Request $request)
	{
		$stock=null;
		$data=$request->all();
		//dd($data['date']);
		foreach ($data['date'] as $keyd => $date) {
			foreach ($data['eat_id'] as $keye => $eat_id) {

				if(isset($data['name'.$date.$eat_id])){
				$stock=new Stock();
	    		$stock->name=$data['name'.$date.$eat_id];
				$stock->date=$date;
				$stock->eat_id=$eat_id;
				if (Auth::check()) {
		            $stock->user_id=Auth::id();
		        }
				$stock->save();
			  }

		    }
		}

	

		if(!is_null($stock)){
			session()->flash('success','Stock create Successfully!!');
			return redirect()->route('student_meal_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
